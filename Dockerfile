ARG BASE_CONTAINER=jupyter/scipy-notebook:python-3.9
FROM $BASE_CONTAINER


RUN pip install --prefer-binary --no-cache-dir \
    "typing-extensions==4.5.0" \
    "nomad-lab[infrastructure,parsing] @ git+https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR.git@develop" \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# RUN pip install --prefer-binary --no-cache-dir --extra-index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple \
#     "nomad-lab[infrastructure,parsing]==1.2.0rc0" \
#     "typing-extensions==4.5.0" \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"


USER ${NB_UID}
WORKDIR "${HOME}"

COPY --chown=${NB_UID}:${NB_GID} notebooks/querying_GW.ipynb .

ENV DOCKER_STACKS_JUPYTER_CMD="nbclassic"
