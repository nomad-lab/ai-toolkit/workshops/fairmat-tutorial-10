# Fairmat Tuturial 10: FAIR electronic-structure data in NOMAD

## Run the docker image locally:
To install docker please follow the instructions provided [here](https://docs.docker.com/engine/install/).

```bash
docker run -it --rm -p 8888:8888 gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/workshop-fairmat-tutorial-10
```
Then open the supplied link into a browser. 

